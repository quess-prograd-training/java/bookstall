public class Book {
    private int bookPrn;
    private String bookName;
    private int price;

    public Book(int bookPrn, String bookName, int i, int i1) {
        this.bookPrn = bookPrn;
        this.bookName = bookName;
    }

    public int getBookPrn() {
        return bookPrn;
    }

    public String getBookName() {
        return bookName;
    }

    public int getPrice() {
        return price;
    }
}

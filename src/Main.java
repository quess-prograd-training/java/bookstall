public class Main {
    public static void main(String[] args) {
        BookStall bookStallObject = new BookStall();
        Items itemsObject = new Items(1,"book1",4,120);
        Items itemsObject1 = new Items(2,"book2",0,130);
        Items itemsObject2 = new Items(3,"book3",4,150);
        bookStallObject.addToCart(itemsObject);
        bookStallObject.addToCart(itemsObject1);
        bookStallObject.addToCart(itemsObject2);
        bookStallObject.printCart();
    }
}


public class Items extends Book {
     private int stock;
     private int price;

    public Items(int bookPrn, String bookName, int stock,int price) {
        super(bookPrn, bookName, 5, 10);
        this.stock = stock;
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

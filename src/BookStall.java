import java.util.HashMap;
import java.util.HashSet;

public class BookStall {
    private HashMap<Integer,Book> books;
    private HashSet<Book> cart;
    private int cartAmount;

    public BookStall() {
        this.books = new HashMap<>();
        this.cart = new HashSet<>();
        this.cartAmount = 0;
    }
    public void addBook(Book book){
        books.put(book.getBookPrn(),book);
    }
    public Book getBook(int bookPrn){
        return books.get(bookPrn);

    }
    public void addToCart(Items items){
        if (items.getStock() > 0) {
            boolean found = false;
            for (Book b : cart) {
                if (b.getBookPrn() == items.getBookPrn()) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                cart.add(items);
                    items.setStock(items.getStock() - 1);
                cartAmount += items.getPrice();
            } else {
                System.out.println("Book is already in cart!");
            }
        } else {
            System.out.println("Sorry, book is out of stock!");
        }
    }
    public void printCart() {
        System.out.println("Books in cart: ");
        for (Book book : cart) {
            System.out.println("Book ID: " + book.getBookPrn() + ", Book Name: " + book.getBookName() + ", Price: " + book.getPrice());
        }
        System.out.println("Total amount: " + cartAmount);
    }
}
